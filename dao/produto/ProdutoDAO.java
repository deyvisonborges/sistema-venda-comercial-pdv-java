package com.deeborges.dao.produto;


import com.deeborges.interfaces.IDAO;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import com.deeborges.model.produto.Produto;
import com.deeborges.utils.Conexao;


public class ProdutoDAO implements IDAO<Produto> {

    @Override
    public void inserir(Produto produto) throws Exception {
        Conexao c = new Conexao();
        String sql = "INSERT INTO produtos (nome, descricao, preco_compra, preco_venda, data_entrada, quantidade) "
                + "VALUES (?, ?, ?, ?, ?, 0)";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, produto.getNome());
        ps.setString(2, produto.getDescricao());
        ps.setDouble(3, produto.getPrecoCompra());
        ps.setDouble(4, produto.getPrecoVenda());
        ps.setDate(5, (Date) produto.getDataEntrada()); // (Date) é o mesmo que 'new Date'
        ps.setInt(6, produto.getQuantidade());
        ps.execute();
        c.confirmar();
    }

    
    @Override
    public void alterar(Produto produto) throws Exception {
        Conexao c = new Conexao();
        String sql = "UPDATE produtos SET "
                + "nome=?, descricao=? preco_compra=?, preco_venda=?, data_entrada=?, quantidade=?"
                + " WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, produto.getNome());
        ps.setString(2, produto.getDescricao());
        ps.setDouble(3, produto.getPrecoCompra());
        ps.setDouble(4, produto.getPrecoVenda());
        ps.setDate(5, (Date) produto.getDataEntrada()); // (Date) é o mesmo que 'new Date'
        ps.setInt(6, produto.getQuantidade());
        ps.execute();
        c.confirmar();
    }
    

    public void entradaEstoque(Conexao c, int codigo, int quantidade) throws Exception {
        String sql = "UPDATE produtos SET QUANTIDADEESTOQUE= QUANTIDADEESTOQUE  + ? WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, quantidade);
        ps.setInt(2, codigo);
        ps.execute();
    }
    

    public void saidaEstoque(Conexao c, int codigo, int quantidade) throws Exception {
        String sql = "UPDATE produtos SET QUANTIDADEESTOQUE= QUANTIDADEESTOQUE  - ? WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, quantidade);
        ps.setInt(2, codigo);
        ps.execute();
    }
    

    @Override
    public void excluir(Produto produto) throws Exception {
        Conexao c = new Conexao();
        String sql = "DELETE FROM produtos WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, produto.getCodigo());
        ps.execute();
        c.confirmar();
    }
    

    @Override
    public ArrayList<Produto> listarTodos() throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM produtos ORDER BY nome";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        ArrayList listaProdutos = new ArrayList();
        while (rs.next()) {
            Produto produto = new Produto();
            produto.setCodigo(rs.getInt("codigo"));
            produto.setNome(rs.getString("nome"));
            produto.setDescricao(rs.getString("descricao"));
            produto.setPrecoCompra(rs.getDouble("preco_compra"));
            produto.setPrecoVenda(rs.getDouble("preco_venda"));
            produto.setDataEntrada(rs.getDate("data_entrada"));
            produto.setQuantidade(rs.getInt("quantidade"));
            listaProdutos.add(produto);
        }

        return listaProdutos;
    }

    
    @Override
    public Produto recuperar(int codigo) throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM produtos WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, codigo);
        ResultSet rs = ps.executeQuery();

        Produto produto = new Produto();
        if (rs.next()) {
            produto.setCodigo(rs.getInt("codigo"));
            produto.setNome(rs.getString("nome"));
            produto.setDescricao(rs.getString("descricao"));
            produto.setPrecoCompra(rs.getDouble("preco_compra"));
            produto.setPrecoVenda(rs.getDouble("preco_venda"));
            produto.setDataEntrada(rs.getDate("data_entrada"));
            produto.setQuantidade(rs.getInt("quantidade"));
        }

        return produto;
    }
}
