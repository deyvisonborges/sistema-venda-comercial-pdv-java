package com.deeborges.dao.localidade;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.deeborges.interfaces.IDAO;
import com.deeborges.model.localidade.Bairro;
import com.deeborges.utils.Conexao;


public class BairroDAO implements IDAO<Bairro> {

    @Override
    public void inserir(Bairro bairro) throws Exception {
        Conexao c = new Conexao();
        String sql = "INSERT INTO bairros (nome, cod_cidade) VALUES (?, ?)";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, bairro.getNome());
        ps.setInt(2, bairro.getCodigo_cidade());
        ps.execute();
        c.confirmar();
    }

    
    @Override
    public void alterar(Bairro bairro) throws Exception {
        Conexao c = new Conexao();
        String sql = "UPDATE bairros SET nome=?, cod_cidade=?, WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, bairro.getNome());
        ps.setInt(2, bairro.getCodigo_cidade());
        ps.execute();
        c.confirmar();    
    }
    

    @Override
    public void excluir(Bairro bairro) throws Exception {
        Conexao c = new Conexao();
        String sql = "DELETE FROM bairros WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, bairro.getCodigo());
        ps.execute();
        c.confirmar();
    }
    

    @Override
    public ArrayList<Bairro> listarTodos() throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM bairros ORDER BY nome";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        ArrayList listaClientes = new ArrayList();
        while (rs.next()) {
            Bairro bairro = new Bairro();
            bairro.setCodigo(rs.getInt("codigo"));
            bairro.setNome(rs.getString("nome"));
            bairro.setCodigo_cidade(rs.getInt("cod_cidade"));
            listaClientes.add(bairro);
        }

        return listaClientes;
    }
    

    @Override
    public Bairro recuperar(int codigo) throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM bairros WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, codigo);
        ResultSet rs = ps.executeQuery();

        Bairro bairro = new Bairro();
        if (rs.next()) {
            bairro.setCodigo(rs.getInt("codigo"));
            bairro.setNome(rs.getString("nome"));
            bairro.setCodigo_cidade(rs.getInt("cod_cidade"));
        }

        return bairro;
    }
    
}
