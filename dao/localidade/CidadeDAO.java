package com.deeborges.dao.localidade;

import com.deeborges.interfaces.IDAO;
import com.deeborges.model.localidade.Cidade;
import com.deeborges.utils.Conexao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class CidadeDAO implements IDAO<Cidade>{

    @Override
    public void inserir(Cidade cidade) throws Exception {
        Conexao c = new Conexao();
        String sql = "INSERT INTO cidades (nome, estado) VALUES (?, ?)";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, cidade.getNome());
        ps.setString(2, cidade.getEstado());
        ps.execute();
        c.confirmar();
    }

    
    @Override
    public void alterar(Cidade cidade) throws Exception {
        Conexao c = new Conexao();
        String sql = "UPDATE cidades SET nome=?, estado=?, WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setString(1, cidade.getNome());
        ps.setString(2, cidade.getEstado());
        ps.execute();
        c.confirmar();    
    }
    

    @Override
    public void excluir(Cidade cidade) throws Exception {
        Conexao c = new Conexao();
        String sql = "DELETE FROM cidades WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, cidade.getCodigo());
        ps.execute();
        c.confirmar();
    }
    

    @Override
    public ArrayList<Cidade> listarTodos() throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM cidades ORDER BY nome";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        ArrayList listaClientes = new ArrayList();
        while (rs.next()) {
            Cidade cidade = new Cidade();
            cidade.setCodigo(rs.getInt("codigo"));
            cidade.setNome(rs.getString("nome"));
            cidade.setEstado(rs.getString("estado"));
            listaClientes.add(cidade);
        }

        return listaClientes;
    }
    

    @Override
    public Cidade recuperar(int codigo) throws Exception {
        Conexao c = new Conexao();
        String sql = "SELECT * FROM cidades WHERE codigo=?";
        PreparedStatement ps = c.getConexao().prepareStatement(sql);
        ps.setInt(1, codigo);
        ResultSet rs = ps.executeQuery();

        Cidade cidade = new Cidade();
        if (rs.next()) {
            cidade.setCodigo(rs.getInt("codigo"));
            cidade.setNome(rs.getString("nome"));
            cidade.setEstado(rs.getString("estado"));
        }

        return cidade;
    }
}
