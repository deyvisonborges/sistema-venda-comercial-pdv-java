package com.deeborges.model.produto;

import java.util.ArrayList;

public class Categoria {
    private int codigo;
    private ArrayList<String> categorias;
    
    public Categoria() {
        this.codigo = 0;
        this.categorias = new ArrayList<>();
    }
       
    public void addCategoria(String nome) {
        this.getCategorias().add(nome);
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public ArrayList<String> getCategorias() {
        return categorias;
    }

    public void setCategorias(ArrayList<String> categorias) {
        this.categorias = categorias;
    }
}
