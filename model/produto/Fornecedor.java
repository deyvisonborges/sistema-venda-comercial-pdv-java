package com.deeborges.model.produto;

public class Fornecedor {
    private int codigo;
    private String nome;
    private String CNPJ;
    
    public Fornecedor() {
        this.codigo = 0;
        this.nome = "";
        this.CNPJ = "";
    }

    public Fornecedor(int codigo) {
        this.codigo = codigo;
        this.nome = "";
        this.CNPJ = "";
    }


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }
    
    
}
