package com.deeborges.model.entidades;

import java.util.Date;

public class Cliente extends Pessoa{
    private int codigo;
    private Date clienteDesde;
    
    public Cliente() {
        this.codigo = 0;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getClienteDesde() {
        return clienteDesde;
    }

    public void setClienteDesde(Date clienteDesde) {
        this.clienteDesde = clienteDesde;
    }
}



